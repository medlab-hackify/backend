const express = require('express')
const cors = require('cors')
const app = express()

//? Configuration app
app.set('port', 4000)

//? Middlewares
app.use(cors())
app.use(express.json())

//? Routes
app.use('/', (req, res)=>{res.json({message:"Hola Mundo!"})}) //! Route hello world, not send in production.

module.exports = app;